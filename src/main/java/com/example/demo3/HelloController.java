package com.example.demo3;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/hello")
public class HelloController {

    @RequestMapping("/test")
    public String test() {
        return "hello/test";
    }

    @RequestMapping("/test2")
    public ModelAndView test2() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("hello/test2");
        return mv;
    }

}
